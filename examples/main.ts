/*
 * File: main.ts
 * Project: Curso de NodeJS Completo - Cod3r
 * File Created: Tuesday, 19 May 2020 12:03:21
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 19 May 2020 15:04:27
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

import {fatorial} from './fatorial';
import * as yargs from 'yargs';

const argv = yargs
    .alias('n','num')
    .demandOption('num')
    .argv; // legal(!!) atribui o valor do argumento a referencia "num" (ex: node main --num=6).

const num = argv.num;

console.log(`O fatorial de ${num} é igual a ${fatorial(num)}`);