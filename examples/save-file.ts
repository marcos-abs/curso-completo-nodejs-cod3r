/*
 * File: save-file.ts
 * Project: Curso de NodeJS Completo - Cod3r
 * File Created: Tuesday, 19 May 2020 12:44:43
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 19 May 2020 17:10:50
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

import * as fs from 'fs';
import * as yargs from 'yargs';

const argv = yargs
    .alias('f','filename')
    .alias('c','content')
    .demandOption('filename')
    .demandOption('content')
    .argv;

fs.writeFile(argv.filename, argv.content, (error) => {
    if(error) throw error;
    console.log(`Arquivo ${argv.filename} foi salvo com sucesso.`);
});