/*
 * File: index.js
 * Project: cluster-examples
 * File Created: Thursday, 23 July 2020 14:52:58
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 23 July 2020 16:53:13
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */

const restify = require('restify');

let app = restify.createServer();
app.use(restify.plugins.queryParser());

app.get('/', (req, resp, next) => {
    setTimeout(() => {
        resp.json({pid: process.pid, echo: req.query});
    }, 500);
});

app.listen(4000, () => {
    console.log('Listening on port 4000');
});