/*
 * File: merge-patch.parser.ts
 * Project: meat-api
 * File Created: Friday, 03 July 2020 20:01:24
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Sunday, 05 July 2020 16:58:00
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */
import * as restify from 'restify';
import { BadRequestError } from 'restify-errors'

const mpContentType = 'application/merge-patch+json';

export const mergePatchBodyParser = (req: restify.Request, resp: restify.Response, next) => {
    if(req.getContentType() === mpContentType && req.method === 'PATCH') {
        (<any>req).rawBody = req.body;
        try {
            req.body = JSON.parse(req.body);
        } catch (e) {
            return next(new BadRequestError(`Invalid content: ${e.message}`));
        }
    }
    return next();
};