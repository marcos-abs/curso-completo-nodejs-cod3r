/*
 * File: server.ts
 * Project: Curso de NodeJS Completo - Cod3r
 * File Created: Wednesday, 20 May 2020 12:04:51
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 25 July 2020 16:44:25
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

import * as fs from 'fs';
import * as restify from 'restify';
import * as mongoose from 'mongoose';
import * as corsMiddleware from 'restify-cors-middleware';

import { environment } from '../common/environment';
import { logger } from '../common/logger';
import { Router } from '../common/router';
import { mergePatchBodyParser } from './merge-patch.parser';
import { handleError } from './errorhandler';

import { tokenParser } from '../security/token.parser';

export class Server {
    application: restify.Server;

    initializeDB(): mongoose.MongooseThenable { // ": mongoose.MongooseThenable" ==> para retornar um promise.
        (<any>mongoose).Promise = global.Promise;
        return mongoose.connect(environment.db.url, {
            useMongoClient: true
        });
    }

    initRoutes(routers: Router[] = []): Promise<any> {
        return new Promise((resolve, reject) => {
            try {

                // Parâmetros do servidor
                const options: restify.ServerOptions = {
                    name: 'meat-api',
                    version: '1.0.0',
                    log: logger
                };

                // Parâmetros do servidor - continuação - definidos em /common/environment.js
                if(environment.security.enableHTTPS) {
                    options.certificate = fs.readFileSync(environment.security.certificate);     // criptografia https parte 1 de 2
                    options.key         = fs.readFileSync(environment.security.key);             // criptografia https parte 2 de 2
                };

                // Criação do servidor
                this.application = restify.createServer(options);

                // CORS - Cross-Origin Resource Sharing - parte 1 de 2
                const corsOptions: corsMiddleware.Options = {
                    preflightMaxAge: 86400, // tempo (em segundos) de espera para um novo preflight do navegador.
                    origins: ['*'], //cuidado! ao liberar para todos os domínios.
                    allowHeaders: ['authorization'],
                    exposeHeaders: ['x-custom-header']
                };
                const cors: corsMiddleware.CorsMiddleware = corsMiddleware(corsOptions);
                this.application.pre(cors.preflight);

                this.application.pre(restify.plugins.requestLogger({
                    log: logger
                }));

                // CORS - Cross-Origin Resource Sharing - parte 2 de 2
                this.application.use(cors.actual);

                // Plugins etc. (continuação)
                this.application.use(restify.plugins.queryParser());
                this.application.use(restify.plugins.bodyParser());
                this.application.use(mergePatchBodyParser);
                this.application.use(tokenParser);

                // Routes
                for (let router of routers) {
                    router.applyRoutes(this.application);
                }

                // Listener
                this.application.listen(environment.server.port, () => {
                    resolve(this.application);
                });

                // Error Handler
                this.application.on('restifyError', handleError);

                // Audit Logger
                /* Desativado - definir qual a melhor forma de personalizar o Audit Logger.
                this.application.on('after', restify.plugins.auditLogger({
                    log: logger,
                    event: 'after',
                    server: this.application
                }));

                this.application.on('audit', data => {
                    // configurações de auditoria personalizada para não mostrar os tokens de autenticação.
                });
                */

            } catch (error) {
                reject(error);
            }
        });
    }

    bootstrap(routers: Router[] = []): Promise<Server> {
        return this.initializeDB().then(() => this.initRoutes(routers).then(() => this));
    }

    shutdown() {
        return mongoose.disconnect().then(() => this.application.close());
    }
}