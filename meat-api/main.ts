/*
 * File: main.ts
 * Project: Curso de NodeJS Completo - Cod3r
 * File Created: Tuesday, 19 May 2020 19:45:47
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 07 July 2020 13:28:04
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
import { Server } from './server/server';
import { usersRouter } from './users/users.router';
import { restaurantsRouter } from './restaurants/restaurants.router';
import { reviewsRouter } from './reviews/reviews.router';
import { mainRouter } from './main.router';

const server = new Server();
server.bootstrap([
    usersRouter,
    restaurantsRouter,
    reviewsRouter,
    mainRouter
]).then(server => {
    console.log('Server is listening on:', server.application.address());
}).catch(error => {
    console.log('Server failed to start');
    console.error(error);
    process.exit(1);
});