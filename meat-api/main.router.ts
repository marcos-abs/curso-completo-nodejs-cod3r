/*
 * File: main.router.ts
 * Project: meat-api
 * File Created: Thursday, 09 July 2020 19:04:23
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 09 July 2020 19:04:23
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */

import {Router} from './common/router'
import * as restify from 'restify'

class MainRouter extends Router {
  applyRoutes(application: restify.Server) {
    application.get('/', (req, resp, next)=>{
      resp.json({
        users: '/users',
        restaurants: '/restaurants',
        reviews: '/reviews'
      })
    })
  }
}

export const mainRouter = new MainRouter()