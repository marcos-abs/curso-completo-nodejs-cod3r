/*
 * File: users.router.ts
 * Project: Curso de NodeJS Completo - Cod3r
 * File Created: Wednesday, 20 May 2020 12:48:23
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Sunday, 19 July 2020 13:06:59
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

import { ModelRouter } from '../common/model-router';
import * as restify from 'restify';
import { NotFoundError } from 'restify-errors';
import { User } from './users.model';
import { authenticate } from '../security/auth.handler';
import { authorize } from '../security/authz.handler';

class UsersRouter extends ModelRouter<User> {

    constructor() {
        super(User);
        this.on('beforeRender', document => {
            document.password = undefined;
            //delete document.password.
        });
    }

    findByEmail = (req, resp, next) => {
        if(req.query.email) {
            User.findByEmail(req.query.email)
                .then(user => user ? [user] : []) // para transformar o resultado da promise anterior em array utilizando operador ternário (!).
                .then(this.renderAll(resp, next, {
                    pageSize: this.pageSize,
                    url: req.url
                }))
                .catch(next);
        } else {
            next();
        }
    };

    applyRoutes(application: restify.Server) {
        application.get({path:`${this.basePath}`, version:'2.0.0'}, [
            authorize('admin'),  // para ativar a autenticação descomentar esta linha.
            this.findByEmail, this.findAll]);
        application.get({path:`${this.basePath}`, version:'1.0.0'}, [authorize('admin'), this.findAll]);
        application.get({path:`${this.basePath}/:id`, version:'1.0.0'}, [authorize('admin'), this.validateId, this.findById]);
        application.post({path:`${this.basePath}`, version:'1.0.0'}, [
            authorize('admin'), // para ativar a autenticação descomentar esta linha.
            this.save]);
        application.put({path:`${this.basePath}/:id`, version:'1.0.0'}, [authorize('admin'), this.validateId, this.replace]);
        application.patch({path:`${this.basePath}/:id`, version:'1.0.0'}, [
            authorize('admin'),   // para ativar a autenticação descomentar esta linha.
            this.validateId, this.update]);
        application.del({path:`${this.basePath}/:id`, version:'1.0.0'}, [authorize('admin'), this.validateId, this.delete]);

        application.post(`${this.basePath}/authenticate`, authenticate);
   }
}

export const usersRouter = new UsersRouter();