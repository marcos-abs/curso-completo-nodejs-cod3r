"use strict";
/*
 * File: logger.ts
 * Project: meat-api
 * File Created: Friday, 24 July 2020 11:49:06
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 24 July 2020 11:49:06
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */
Object.defineProperty(exports, "__esModule", { value: true });
const bunyan = require("bunyan");
const environment_1 = require("./environment");
exports.logger = bunyan.createLogger({
    name: environment_1.environment.log.name,
    level: bunyan.resolveLevel(environment_1.environment.log.level)
});
//# sourceMappingURL=logger.js.map