"use strict";
/*
 * File: users.router.ts
 * Project: Curso de NodeJS Completo - Cod3r
 * File Created: Wednesday, 20 May 2020 12:48:23
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Sunday, 19 July 2020 13:06:59
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
Object.defineProperty(exports, "__esModule", { value: true });
const model_router_1 = require("../common/model-router");
const users_model_1 = require("./users.model");
const auth_handler_1 = require("../security/auth.handler");
const authz_handler_1 = require("../security/authz.handler");
class UsersRouter extends model_router_1.ModelRouter {
    constructor() {
        super(users_model_1.User);
        this.findByEmail = (req, resp, next) => {
            if (req.query.email) {
                users_model_1.User.findByEmail(req.query.email)
                    .then(user => user ? [user] : []) // para transformar o resultado da promise anterior em array utilizando operador ternário (!).
                    .then(this.renderAll(resp, next, {
                    pageSize: this.pageSize,
                    url: req.url
                }))
                    .catch(next);
            }
            else {
                next();
            }
        };
        this.on('beforeRender', document => {
            document.password = undefined;
            //delete document.password.
        });
    }
    applyRoutes(application) {
        application.get({ path: `${this.basePath}`, version: '2.0.0' }, [
            authz_handler_1.authorize('admin'),
            this.findByEmail, this.findAll
        ]);
        application.get({ path: `${this.basePath}`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.findAll]);
        application.get({ path: `${this.basePath}/:id`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.validateId, this.findById]);
        application.post({ path: `${this.basePath}`, version: '1.0.0' }, [
            authz_handler_1.authorize('admin'),
            this.save
        ]);
        application.put({ path: `${this.basePath}/:id`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.validateId, this.replace]);
        application.patch({ path: `${this.basePath}/:id`, version: '1.0.0' }, [
            authz_handler_1.authorize('admin'),
            this.validateId, this.update
        ]);
        application.del({ path: `${this.basePath}/:id`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.validateId, this.delete]);
        application.post(`${this.basePath}/authenticate`, auth_handler_1.authenticate);
    }
}
exports.usersRouter = new UsersRouter();
//# sourceMappingURL=users.router.js.map