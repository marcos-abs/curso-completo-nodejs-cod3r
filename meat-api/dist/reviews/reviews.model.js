"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * File: reviews.model.ts
 * Project: meat-api
 * File Created: Wednesday, 08 July 2020 13:01:22
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 08 July 2020 13:01:22
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */
const mongoose = require("mongoose");
const reviewSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: true
    },
    rating: {
        type: Number,
        required: true
    },
    comments: {
        type: String,
        required: true,
        maxlength: 500
    },
    restaurant: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Restaurant',
        required: false // fonte: https://www.udemy.com/course/nodejs-rest-pt/learn/lecture/9744812#questions/5517988
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});
exports.Review = mongoose.model('Review', reviewSchema);
//# sourceMappingURL=reviews.model.js.map