"use strict";
/*
 * File: server.ts
 * Project: Curso de NodeJS Completo - Cod3r
 * File Created: Wednesday, 20 May 2020 12:04:51
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 25 July 2020 16:44:25
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const restify = require("restify");
const mongoose = require("mongoose");
const corsMiddleware = require("restify-cors-middleware");
const environment_1 = require("../common/environment");
const logger_1 = require("../common/logger");
const merge_patch_parser_1 = require("./merge-patch.parser");
const errorhandler_1 = require("./errorhandler");
const token_parser_1 = require("../security/token.parser");
class Server {
    initializeDB() {
        mongoose.Promise = global.Promise;
        return mongoose.connect(environment_1.environment.db.url, {
            useMongoClient: true
        });
    }
    initRoutes(routers = []) {
        return new Promise((resolve, reject) => {
            try {
                // Parâmetros do servidor
                const options = {
                    name: 'meat-api',
                    version: '1.0.0',
                    log: logger_1.logger
                };
                // Parâmetros do servidor - continuação - definidos em /common/environment.js
                if (environment_1.environment.security.enableHTTPS) {
                    options.certificate = fs.readFileSync(environment_1.environment.security.certificate); // criptografia https parte 1 de 2
                    options.key = fs.readFileSync(environment_1.environment.security.key); // criptografia https parte 2 de 2
                }
                ;
                // Criação do servidor
                this.application = restify.createServer(options);
                // CORS - Cross-Origin Resource Sharing - parte 1 de 2
                const corsOptions = {
                    preflightMaxAge: 86400,
                    origins: ['*'],
                    allowHeaders: ['authorization'],
                    exposeHeaders: ['x-custom-header']
                };
                const cors = corsMiddleware(corsOptions);
                this.application.pre(cors.preflight);
                this.application.pre(restify.plugins.requestLogger({
                    log: logger_1.logger
                }));
                // CORS - Cross-Origin Resource Sharing - parte 2 de 2
                this.application.use(cors.actual);
                // Plugins etc. (continuação)
                this.application.use(restify.plugins.queryParser());
                this.application.use(restify.plugins.bodyParser());
                this.application.use(merge_patch_parser_1.mergePatchBodyParser);
                this.application.use(token_parser_1.tokenParser);
                // Routes
                for (let router of routers) {
                    router.applyRoutes(this.application);
                }
                // Listener
                this.application.listen(environment_1.environment.server.port, () => {
                    resolve(this.application);
                });
                // Error Handler
                this.application.on('restifyError', errorhandler_1.handleError);
                // Audit Logger
                /* Desativado - definir qual a melhor forma de personalizar o Audit Logger.
                this.application.on('after', restify.plugins.auditLogger({
                    log: logger,
                    event: 'after',
                    server: this.application
                }));

                this.application.on('audit', data => {
                    // configurações de auditoria personalizada para não mostrar os tokens de autenticação.
                });
                */
            }
            catch (error) {
                reject(error);
            }
        });
    }
    bootstrap(routers = []) {
        return this.initializeDB().then(() => this.initRoutes(routers).then(() => this));
    }
    shutdown() {
        return mongoose.disconnect().then(() => this.application.close());
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map