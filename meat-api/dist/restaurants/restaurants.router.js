"use strict";
/*
 * File: restaurants.router.ts
 * Project: meat-api
 * File Created: Tuesday, 07 July 2020 13:23:09
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 09 July 2020 18:47:34
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */
Object.defineProperty(exports, "__esModule", { value: true });
const model_router_1 = require("../common/model-router");
const restify_errors_1 = require("restify-errors");
const restaurants_model_1 = require("./restaurants.model");
const authz_handler_1 = require("../security/authz.handler");
class RestaurantsRouter extends model_router_1.ModelRouter {
    constructor() {
        super(restaurants_model_1.Restaurant);
        this.findMenu = (req, resp, next) => {
            restaurants_model_1.Restaurant.findById(req.params.id, "+menu")
                .then(rest => {
                if (!rest) {
                    throw new restify_errors_1.NotFoundError('Restaurant not found.');
                }
                else {
                    resp.json(rest.menu);
                    return next();
                }
            }).catch(next);
        };
        this.replaceMenu = (req, resp, next) => {
            restaurants_model_1.Restaurant.findById(req.params.id).then(rest => {
                if (!rest) {
                    throw new restify_errors_1.NotFoundError('Restaurant not found.');
                }
                else {
                    rest.menu = req.body; // Array de MenuItem
                    return rest.save();
                }
            }).then(rest => {
                resp.json(rest.menu);
                return next();
            }).catch(next);
        };
    }
    envelope(document) {
        let resource = super.envelope(document);
        resource._links.menu = `${this.basePath}/${resource._id}/menu`;
        return resource;
    }
    applyRoutes(application) {
        application.get({ path: `${this.basePath}`, version: '1.0.0' }, this.findAll);
        application.get({ path: `${this.basePath}/:id`, version: '1.0.0' }, [this.validateId, this.findById]);
        application.post({ path: `${this.basePath}`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.save]);
        application.put({ path: `${this.basePath}/:id`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.validateId, this.replace]);
        application.patch({ path: `${this.basePath}/:id`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.validateId, this.update]);
        application.del({ path: `${this.basePath}/:id`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.validateId, this.delete]);
        application.get({ path: `${this.basePath}/:id/menu`, version: '1.0.0' }, [this.validateId, this.findMenu]);
        application.put({ path: `${this.basePath}/:id/menu`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.validateId, this.replaceMenu]);
    }
}
exports.restaurantsRouter = new RestaurantsRouter();
//# sourceMappingURL=restaurants.router.js.map