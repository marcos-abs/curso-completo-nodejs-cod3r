"use strict";
/*
 * File: main.router.ts
 * Project: meat-api
 * File Created: Thursday, 09 July 2020 19:04:23
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 09 July 2020 19:04:23
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */
Object.defineProperty(exports, "__esModule", { value: true });
const router_1 = require("./common/router");
class MainRouter extends router_1.Router {
    applyRoutes(application) {
        application.get('/', (req, resp, next) => {
            resp.json({
                users: '/users',
                restaurants: '/restaurants',
                reviews: '/reviews'
            });
        });
    }
}
exports.mainRouter = new MainRouter();
//# sourceMappingURL=main.router.js.map