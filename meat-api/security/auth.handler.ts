/*
 * File: auth.handler.ts
 * Project: meat-api
 * File Created: Tuesday, 14 July 2020 12:35:08
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Sunday, 19 July 2020 12:55:25
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */
import * as restify from 'restify';
import * as jwt from 'jsonwebtoken';
import { NotAuthorizedError } from 'restify-errors';
import { User } from '../users/users.model';
import { environment } from '../common/environment';

export const authenticate: restify.RequestHandler = (req, resp, next) => {
    const {email, password} = req.body;
    User.findByEmail(email, '+password').then(user => {
        if(user && user.matches(password)) {
            const token = jwt.sign({sub: user.email, iss: 'meat-api'},
                environment.security.apiSecret);
            resp.json({name: user.name, email: user.email, accessToken: token});
            return next(false);
        } else {
            return next(new NotAuthorizedError('Invalid credentials'));
        }
    }).catch(next);
};