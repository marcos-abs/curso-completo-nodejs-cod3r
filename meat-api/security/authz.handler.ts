/*
 * File: authz.handler.ts
 * Project: meat-api
 * File Created: Tuesday, 21 July 2020 13:51:04
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 21 July 2020 13:51:05
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */

import * as restify from 'restify';
import { ForbiddenError } from 'restify-errors';

export const authorize: (...profiles: string[]) => restify.RequestHandler = (...profiles) => {
    return (req, resp, next) => {
        if(req.authenticated !== undefined && req.authenticated.hasAny(...profiles)) {
            req.log.debug('User %s is authorized with profiles %j on route %s. Require profiles %j',
                req.authenticated._id,
                req.authenticated.profiles,
                req.path(),
                profiles);
            next();
        } else {
            if(req.authenticated) {
                req.log.debug('Permission denied for %s. Required profiles: %j. User profiles: %j',
                    req.authenticated._id, profiles, req.authenticated.profiles);
            }
            next(new ForbiddenError('Permission denied'));
        }
    }
}