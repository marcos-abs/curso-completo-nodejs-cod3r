/*
 * File: token.parser.ts
 * Project: meat-api
 * File Created: Monday, 20 July 2020 13:44:58
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Monday, 20 July 2020 13:44:58
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */

import * as restify from 'restify';
import * as jwt from 'jsonwebtoken';
import { User } from '../users/users.model';
import { environment } from '../common/environment';
import { authenticate } from './auth.handler';

export const tokenParser: restify.RequestHandler = (req, resp, next) => {
    const token = extractToken(req);
    if(token) {
        jwt.verify(token, environment.security.apiSecret, applyBearer(req, next));
    } else {
        next();
    }
}

function extractToken(req: restify.Request) {
    let token = undefined;
    const authorization = req.header('authorization');
    if(authorization) {
        const parts: string[] = authorization.split(' ');
        if(parts.length === 2 && parts[0] === 'Bearer') {
            token = parts[1];
        }
    }
    return token;
}

function applyBearer (req: restify.Request, next): (error, decoded) => void {
    return (error, decoded) => {
        if(decoded) {
            User.findByEmail(decoded.sub).then(user => {
                if(user) {
                    // associar o usuário no request
                    req.authenticated = user;
                }
                next();
            });
        } else {
            next();
        }
    }
}