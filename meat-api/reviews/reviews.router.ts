/*
 * File: reviews.router.ts
 * Project: meat-api
 * File Created: Wednesday, 08 July 2020 13:19:29
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 09 July 2020 18:47:44
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */

import * as restify from 'restify';
import * as mongoose from 'mongoose';
import { ModelRouter } from '../common/model-router';
import { Review } from './reviews.model';
import { restaurantsRouter } from '../restaurants/restaurants.router';
import { authorize } from '../security/authz.handler';

class ReviewsRouter extends ModelRouter<Review> {

    constructor() {
        super(Review);
    }

    /**
     * Estratégia 1 : modificar somente o método findById no Review para "popular" os valores do usuário e do restaurante do review.
     *
     *     findById = (req, resp, next) => {
     *         this.model.findById(req.params.id)
     *             .populate('user', 'name')
     *             .populate('restaurant', 'name')
     *             .then(this.render(resp, next))
     *             .catch(next);
     *     }
     *
     */

    /**
     * Estratégia 2 - parte 2 de 2: modificar diretamente no model-router.ts o método findById no Review para "popular" os valores do usuário e do restaurante do review. 
     */
    protected prepareOne(query: mongoose.DocumentQuery<Review,Review>): mongoose.DocumentQuery<Review,Review> {
        return query
            .populate('user', 'name')
            .populate('restaurant', 'name');
       }


    envelope(document) {
        let resource = super.envelope(document);
        const restId = document.restaurant._id ? document.restaurant._id : document.restaurant;
        resource._links.restaurant = `/restaurants/${restId}`;
        return resource;
    }


    applyRoutes(application: restify.Server) {
        application.get({path:`${this.basePath}`, version:'1.0.0'}, this.findAll);
        application.get({path:`${this.basePath}/:id`, version:'1.0.0'}, [this.validateId, this.findById]);
        application.post({path:`${this.basePath}`, version:'1.0.0'}, [authorize('user'),this.save]);
        application.del({path:`${this.basePath}/:id`, version:'1.0.0'}, [authorize('admin'),this.validateId, this.delete]); // deixei para efeito de manutenção do cadastro.
    }
}

export const reviewsRouter = new ReviewsRouter();