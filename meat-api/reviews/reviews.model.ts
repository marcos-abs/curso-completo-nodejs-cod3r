/*
 * File: reviews.model.ts
 * Project: meat-api
 * File Created: Wednesday, 08 July 2020 13:01:22
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 08 July 2020 13:01:22
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */
import * as mongoose from 'mongoose';
import { Restaurant } from '../restaurants/restaurants.model';
import { User } from '../users/users.model';

export interface Review extends mongoose.Document {
    date: Date,
    rating: number,
    comments: string,
    restaurant: mongoose.Types.ObjectId | Restaurant,
    user: mongoose.Types.ObjectId | User
}

const reviewSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: true
    },
    rating: {
        type: Number,
        required: true
    },
    comments: {
        type: String,
        required: true,
        maxlength: 500
    },
    restaurant: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Restaurant',
        required: false // fonte: https://www.udemy.com/course/nodejs-rest-pt/learn/lecture/9744812#questions/5517988
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
})

export const Review = mongoose.model<Review>('Review', reviewSchema);