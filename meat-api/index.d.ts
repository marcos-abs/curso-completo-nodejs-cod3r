/*
 * File: index.d.ts
 * Project: meat-api
 * File Created: Tuesday, 21 July 2020 13:33:38
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Tuesday, 21 July 2020 13:33:38
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */

import { User } from './users/users.model';
import { authenticate } from './security/auth.handler';

declare module 'restify' {
    export interface Request {
        authenticated: User;
    }
}
