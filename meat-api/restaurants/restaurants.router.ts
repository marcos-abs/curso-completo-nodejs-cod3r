/*
 * File: restaurants.router.ts
 * Project: meat-api
 * File Created: Tuesday, 07 July 2020 13:23:09
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 09 July 2020 18:47:34
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */

import { ModelRouter } from '../common/model-router';
import * as restify from 'restify';
import { NotFoundError } from 'restify-errors';
import { Restaurant } from './restaurants.model';
import { authorize } from '../security/authz.handler';

class RestaurantsRouter extends ModelRouter<Restaurant> {
    constructor() {
        super(Restaurant);
    }

    envelope(document) {
        let resource = super.envelope(document);
        resource._links.menu = `${this.basePath}/${resource._id}/menu`;
        return resource;
    }

    findMenu = (req, resp, next) => {
        Restaurant.findById(req.params.id, "+menu")
            .then(rest => {
                if(!rest) {
                    throw new NotFoundError('Restaurant not found.');
                } else {
                    resp.json(rest.menu);
                    return next();
                }
            }).catch(next);
    };

    replaceMenu = (req, resp, next) => {
        Restaurant.findById(req.params.id).then(rest => {
            if(!rest) {
                throw new NotFoundError('Restaurant not found.');
            } else {
                rest.menu = req.body // Array de MenuItem
                return rest.save();
            }
        }).then(rest => {
            resp.json(rest.menu);
            return next();
        }).catch(next);
    };

    applyRoutes(application: restify.Server) {

        application.get({path:`${this.basePath}`, version:'1.0.0'}, this.findAll);
        application.get({path:`${this.basePath}/:id`, version:'1.0.0'}, [this.validateId, this.findById]);
        application.post({path:`${this.basePath}`, version:'1.0.0'}, [authorize('admin'),this.save]);
        application.put({path:`${this.basePath}/:id`, version:'1.0.0'}, [authorize('admin'),this.validateId, this.replace]);
        application.patch({path:`${this.basePath}/:id`, version:'1.0.0'}, [authorize('admin'),this.validateId, this.update]);
        application.del({path:`${this.basePath}/:id`, version:'1.0.0'}, [authorize('admin'),this.validateId, this.delete]);

        application.get({path:`${this.basePath}/:id/menu`, version:'1.0.0'}, [this.validateId, this.findMenu]);
        application.put({path:`${this.basePath}/:id/menu`, version:'1.0.0'}, [authorize('admin'),this.validateId, this.replaceMenu]);
    }
}

export const restaurantsRouter = new RestaurantsRouter();