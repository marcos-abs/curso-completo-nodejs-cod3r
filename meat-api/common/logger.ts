/*
 * File: logger.ts
 * Project: meat-api
 * File Created: Friday, 24 July 2020 11:49:06
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 24 July 2020 11:49:06
 * Modified By: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br>)
 * -----
 * Copyright 2019 - 2020 All rights reserved, Marcant Tecnologia
 */

import * as bunyan from 'bunyan';
import { environment } from './environment';

export const logger = bunyan.createLogger({
    name: environment.log.name,
    level: (<any>bunyan).resolveLevel(environment.log.level)
});